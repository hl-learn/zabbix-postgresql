FROM postgres:9.6
COPY src/create.sql .
ENV POSTGRES_USER postgres
ENV POSTGRES_DB postgres
ENV POSTGRES_PASSWORD postgres
ENV POSTGRES_HOST_AUTH_METHOD password
ADD src/init.sh /docker-entrypoint-initdb.d
ENTRYPOINT ["docker-entrypoint.sh"]
EXPOSE 5432/tcp
CMD ["postgres"]
