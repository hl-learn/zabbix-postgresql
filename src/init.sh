#! /bin/bash

set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE ROLE zabbix WITH PASSWORD 'zabbix';
    CREATE DATABASE zabbix;
    ALTER DATABASE zabbix OWNER TO zabbix;
EOSQL

psql -U zabbix -d zabbix -f create.sql
